package az.ingress.employee.aspect;

import lombok.extern.slf4j.Slf4j;
import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.*;
import org.springframework.stereotype.Component;


@Aspect
@Component
@Slf4j
public class LoggingAspect {

    @Before("execution(* az.ingress.employee.controller.EmployeeController.*(..)) && args(arg,..)")
    public void logBefore(JoinPoint joinPoint, Object arg) {
        log.info("Before " + joinPoint.getSignature().getName() + " is called with argument: " + arg);
    }

    @AfterReturning(
            pointcut = "execution(* az.ingress.employee.controller.EmployeeController.*(..))",
            returning = "result")
    public void logAfter(JoinPoint joinPoint, Object result) {
        log.info("After " + joinPoint.getSignature().getName() + " is called. Result: " + result);
    }

    @After("execution(* az.ingress.employee.controller.EmployeeController.*(..))")
    public void logAfter(JoinPoint joinPoint) {
        log.info("After " + joinPoint.getSignature().getName() + " is called.");
    }

    @Around("execution(* az.ingress.employee.controller.EmployeeController.*(..))")
    public Object logAround(ProceedingJoinPoint joinPoint) throws Throwable {
        log.info("Before " + joinPoint.getSignature().getName() + " is called.");
        Object result = joinPoint.proceed();
        log.info("After " + joinPoint.getSignature().getName() + " is called.");
        return result;
    }

    @AfterThrowing(pointcut = "execution(* az.ingress.employee.controller.EmployeeController.*(..))", throwing = "exception")
    public void logAfterThrowing(JoinPoint joinPoint, Throwable exception) {
        log.error("Exception occurred in method: {}", joinPoint.getSignature().getName());
        log.error("Exception message: {}", exception.getMessage());
    }

    @Around("@annotation(az.ingress.employee.aspect.LogExecutionTime)")
    public Object logExecutionTime(ProceedingJoinPoint joinPoint) throws Throwable {
        long startTime = System.currentTimeMillis();
        Object proceed = joinPoint.proceed();
        long endTime = System.currentTimeMillis();
        String methodName = joinPoint.getSignature().getName();
        String className = joinPoint.getTarget().getClass().getName();
        log.info("Method '{}' in class '{}' executed in {} ms", methodName, className, (endTime - startTime));
        return proceed;
    }
}
